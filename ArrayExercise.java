package array;

import java.util.Arrays;
import java.util.Random;

public class ArrayExercise {

	public static void main(String[] args) {

		//set random number
		Random random = new Random();
		int[] num = new int[10];
		int total = 0;

		for (int i = 0; i < num.length; i++) {
			num[i] = random.nextInt(100);
		}

		System.out.println(Arrays.toString(num));

		// find total & average
		for (int i = 0; i < num.length; i++) {
			total += num[i];
		}
		System.out.println("\nTotal : " + total + "\n");

		int avg = total / 10;
		System.out.println("Average : " + avg + "\n");

		boolean found = false;
		int searchNum = 85;
		for (int x : num) {
			if (x == searchNum) {
				found = true;
				break;
			}
		}
		
		// contains
		System.out.println("Contains searchingNumber ? " + found + "\n");

		int value = 0;
		int x = 0;
		for (int i = 0; i < num.length; i++) {

			if (num[i] == value) {
				x = i;
			} else {
				System.out.println("index not found");
				break;

			}
			System.out.println(x);
		}

		// copy array
		int[] numCopy = new int[num.length];

		for (int i = 0; i < num.length; i++)
			numCopy[i] = num[i];

		System.out.print("\nnum[] : ");		
		for (int i = 0; i < num.length; i++)			
			System.out.print(num[i] + " ");

		System.out.print("\n\nnumCopy[] : ");		
		for (int i = 0; i < numCopy.length; i++)
			System.out.print(numCopy[i] + " ");

		// find max

		int max = 0;
		for (int i = 1; i < num.length; i++) {
			if (num[0] > max) {
				max = num[i];
			} else {
				max = num[0];

			}

			System.out.println("\n\nMax value of num[] is " + max);

		}

	}

}
package array;

import java.util.Random;

public class TDArrayFunction {

	public static void prind2DArray(int[][] tdArray) {
		for (int r = 0; r < tdArray.length; r++) {
			for (int c = 0; c < tdArray[r].length; c++) {
				System.out.print(tdArray[r][c] + " ");
			}
			System.out.println();

		}
	}

	public static void setValuestoArray(int tdArray[][]) {
		Random rand = new Random();
		for (int r = 0; r < tdArray.length; r++) {
			for (int c = 0; c < tdArray[r].length; c++) {

				tdArray[r][c] = rand.nextInt(200);

				System.out.print(tdArray[r][c] + " ");
			}
			System.out.println();
		}
	}
	
	public static void sumofRows(int tdArray[][]) {
		for (int r = 0; r < tdArray.length; r++) {
			int sum = 0;
			for (int c = 0; c < tdArray[r].length; c++) {
				sum = sum + tdArray[r][c];
			}
			
		System.out.println("Sum of row "+ (r+1)+" is = "+sum);
		}
	}

	public static void maxValueofRow(int tdArray[][]) {
		for (int r = 0; r < tdArray.length; r++) {
			int max = tdArray[r][0];
			for (int c = 0; c < tdArray[r].length; c++) {
				if (tdArray[r][c] > max) {
					max = tdArray[r][c];
				}
			}
			
		System.out.println("Maximum value of Row "+ (r+1)+" is = "+max);
		}
		
	}
}

package array;

import functions.MyUserInput;

public class ArrayPro1 {

	static int size = MyUserInput.readNumber("Enter a Number");

	public static void main(String[] args) {
		int sum=0;
		int [] num = {10};
		
		for (int i = 1; i <= size; i++){
			sum = sum+num[i];
			System.out.println(i);
		}

		System.out.println();

	}
}

package array;

public class ArrayBasic {

	public static void main(String[] args) {

		/*
		String cars[] = {"Volvo", "BMW","Alto"}; System.out.println(cars[1]); output
		Alto
		 */

		/*
		int[] myNum = {57,64,68,25}; System.out.println(myNum[0]); //output 68
		 */

		/*
		 String[] cars = {"Volvo", "BMW","Ford"}; cars[0] = "opel";
		 System.out.println(cars[0]); output opel
		 */

		/*
		 int[] myNum = {54,68,25,95,85}; System.out.println(myNum.length); //output 5
		 */

		String[] cars = { "Volvo", "BMW", "Ford" };
		for (int n = 0; n < cars.length; n++) {

			System.out.println(cars[n]);

		}

	}
}

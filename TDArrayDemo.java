package array;

public class TDArrayDemo {

	public static void main(String[] args) {

		TDArrayFunction operate = new TDArrayFunction();

		
		 int tdArray[][] = new int[3][4];
		 tdArray[0][0]=1; tdArray[0][1]=2;
		 tdArray[0][2]=3; tdArray[0][3]=4;
		 tdArray[1][0]=11; tdArray[1][1]=12; 
		 tdArray[1][2]=13; tdArray[1][3]=14;
		 tdArray[2][0]=21; tdArray[2][1]=22; 
		 tdArray[2][2]=23; tdArray[2][3]=24;
		 
/*
		int[][] tdArray = { { 1, 2, 3, 4 }, { 11, 12, 13, 14 }, { 21, 22, 23, 24 } };

		[1][2][3][4]
		[11][12][13][14]
		[21][22][23][24]
*/
    	System.out.println(tdArray[1][2]);
		System.out.println(tdArray[2][2]);

		System.out.println(tdArray.length);

		/*
		for (int i = 0; i < tdArray.length; i++) {
		System.out.print(" "+tdArray[i][0]); 
		System.out.print(" "+tdArray[i][1]);
		System.out.print(" "+tdArray[i][2]); 
		System.out.print(" "+tdArray[i][3]);
		System.out.println();
		}
		 */

		/*
		for (int r = 0; r < tdArray.length; r++) { 
		for (int c = 0; c < tdArray[0].length; c++) { 
		System.out.print(tdArray[r][c] + " "); 
		}
		System.out.println(); 
		}
		 */

//		operate.prind2DArray(tdArray);
//		System.out.println();
//		operate.setValuestoArray(tdArray);
//		System.out.println();
	//	operate.sumofRows(tdArray);
		//System.out.println();
		//operate.maxValueofRow(tdArray);

	}

}

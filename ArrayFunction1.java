package array;

public class ArrayFunction1 {

	public static void printArrays(int array[]) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + ",");
		}
		System.out.println();
	}

	public static void setValueToArray(int array[], int index, int value) {
		array[index] = value;
		printArrays(array);
	}

	public int getValueFromArray(int array[], int index) {
		return array[index];
	}

	public static int[] setAutoValuestoArray(int array[]) {
		for (int i = 0; i < array.length; i++) {
			array[i] = i + 1;
		}
		printArrays(array);
		return array;
	}

	public static int[] setAutoReverseValuestoArray(int array[]) {
		for (int i = 0; i < array.length; i++) {
			array[i] = array.length - i;
		}
		printArrays(array);
		return array;
	}

	public static int[] addArrays(int array1[], int array2[]) {
		for (int i = 0; i < array1.length; i++) {
			array1[i] = array1[i] + array2[i];
		}
		printArrays(array1);
		return array1;
	}

	public static void sumOfArray(int array[]) {
		int sum = 0;
		for (int i : array) {
			sum = sum + i;
		}
		
	}

}